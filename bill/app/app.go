package app

type bill struct {
	nomeCliente    string
	itenConsumidos map[string]float64
	taxaService    int
}

// Criar nova conta
func NewBill(nome string) bill {
	conta := bill{
		nomeCliente:    nome,
		itenConsumidos: map[string]float64{},
		taxaService:    0,
	}
	return conta
}
