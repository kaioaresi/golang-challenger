# Golang challenger

## Criate CLI (cobra)

Install cobra generator

```bash
# Get the CLI Generator
go get github.com/spf13/cobra/cobra
# Create dir for your app
mkdir -p dadjoke && cd dadjoke
# Initialize the go module
go mod init gitlab.com/kaioaresi/golang-challenger/dadjoke
# Initialize the CLI project with cobra
## cobra init --pkg-name gitlab.com/kaioaresi/golang-challenger/dadjoke
cobra init -a gitlab.com/kaioaresi/golang-challenger/dadjoke
# Get the cobra/viper dependencies. This will add to your go.mod file
go get github.com/spf13/cobra
go get github.com/spf13/viper
# Build to make sure we are good.
go build | go run main.go

alias cobra="~/go/bin/cobra-cli"
```


---
# References

- [Install golang](https://go.dev/dl/)
- [Lib cobra](https://github.com/spf13/cobra)
- [Lib cobra readme](https://github.com/spf13/cobra/blob/master/README.md)
- [How to build a CLI tool with Go and Cobra | Go Tutorial](https://www.youtube.com/watch?v=-tO7zSv80UY)
- [pkg http](https://pkg.go.dev/net/http)
