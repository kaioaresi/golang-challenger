package main

import (
	"fmt"
	"log"
	"os"
)

func criaDir(nomeDir string) string {
	criaPath := os.Mkdir(nomeDir, os.ModePerm)

	if _, err := os.Stat(nomeDir); os.IsNotExist(err) {
		if criaPath != nil {
			log.Panic(criaPath)
		}
		return "Diretorio criado!"
	}

	return "Ocorreu um erro na criação do path"
}

func main() {
	fmt.Println(criaDir("kaio-teste"))
}
