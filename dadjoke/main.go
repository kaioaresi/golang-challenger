/*
Copyright © 2022 gitlab.com/kaioaresi/golang-challenger/dadjoke

*/
package main

import "gitlab.com/kaioaresi/golang-challenger/dadjoke/cmd"

func main() {
	cmd.Execute()
}
