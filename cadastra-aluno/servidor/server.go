package servidor

import (
	"cadastra-alunos/banco"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type aluno struct {
	ID      uint32 `json:"id"`
	Nome    string `json:"nome"`
	Materia string `json:"materia"`
}

// Func InsertDB - realizar um insert no banco de dados
func InsertDB(w http.ResponseWriter, r *http.Request) {

	requestBody, err := ioutil.ReadAll(r.Body) // Lendo request vinda do body
	if err != nil {
		w.Write([]byte("Erro ao ler o body"))
		return
	}

	var aluno aluno

	if err = json.Unmarshal(requestBody, &aluno); err != nil {
		w.Write([]byte("Erro na conversão de json para struc aluno"))
		return
	}

	db, err := banco.ConectaDB()
	if err != nil {
		w.Write([]byte("Erro conexão com banco de dados"))
		return
	}

	// Prepare para insert no banco
	statment, err := db.Prepare("insert into alunos (nome, materia) values (?,?)")
	if err != nil {
		w.Write([]byte("Erro no prepare do insert no banco de dados."))
		return
	}

	defer statment.Close()

	insertTable, err := statment.Exec(aluno.Nome, aluno.Materia)
	if err != nil {
		w.Write([]byte("Erro ao realizar insert."))
		return
	}

	idInserido, err := insertTable.LastInsertId()
	if err != nil {
		w.Write([]byte("Erro ao realizar o insert do id"))
		return
	}

	// Return status creatated 201 ok
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(fmt.Sprintf("Usuário id %d, inserido com sucesso!", idInserido)))

}
