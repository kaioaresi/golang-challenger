package main

import (
	"cadastra-alunos/servidor"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()
	router.HandleFunc("/alunos", servidor.InsertDB).Methods(http.MethodPost)

	fmt.Println("Servidor iniciado....")
	http.ListenAndServe(":5000", router)
}
