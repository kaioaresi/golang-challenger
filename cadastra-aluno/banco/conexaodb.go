package banco

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql" // Carregando drivers mysql
)

// Func ConectaDB - realiza conexão com banco dados
func ConectaDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", "app-cadastro:golang123@/escoladev")

	if err != nil {
		return nil, err
	}

	// Ping no banco
	if err = db.Ping(); err != nil {
		return nil, err
	}

	// See "Important settings" section.
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	fmt.Println("Conectado ao banco de dados.....")
	return db, nil

}
