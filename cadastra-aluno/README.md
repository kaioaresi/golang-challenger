# Criando cadastro de alunos

## Criando banco de dados

Em nosso lab, vamos utilizar o kubernetes (kind) com banco mysql, suas configurações estão no dir infra.

### COnfigurando credenciais no banco de dados

```mysql
create database escoladev;
use escoladev;

CREATE TABLE alunos(
	id int auto_increment primary key,
	nome varchar(50) not null,
	materia varchar(50) not null
) ENGINE=INNODB;


CREATE USER 'app-cadastro'@'%'IDENTIFIED BY 'golang123';

GRANT ALL PRIVILEGES on escoladev.* TO 'app-cadastro'@'%';

FLUSH PRIVILEGES;
```

### Lib mysql golang

```bash
go get github.com/go-sql-driver/mysql
```

## Import router lib

```bash
go get github.com/gorilla/mux
```