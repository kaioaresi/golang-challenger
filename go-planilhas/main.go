package main

import (
	"encoding/csv"
	"log"
	"os"
)

func main() {

	// Cria documento
	file, err := os.Create("teste-k.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Escreve no documento
	escreveFile := csv.NewWriter(file)
	defer escreveFile.Flush()

	// Header
	header := []string{"Nome", "Class"}
	escreveFile.Write(header)

	// Multiplos inserts
	alunos := [][]string{
		{"Garen", "tk"},
		{"katariana", "assassino"},
		{"ez", "adc"},
		{"lux", "mage"},
	}

	for _, aluno := range alunos {
		escreveFile.Write(aluno)
	}

	// // Escrita unica - array/slice
	// aluno := []string{"Garen", "garen@garen.com"}
	// escreveFile.Write(aluno)

}
